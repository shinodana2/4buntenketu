const LINE_HEIGHT=1.4;
const N_CHAR_OF_ONE_ROW = 20;

const BAN_FOOT = '，、。．.・：；？！―…ー−→←↑↓）)」』〕｝】々んぁぃぅぇぉっゃゅょァィゥェォッャュョ';
const BAN_HEAD = "（(「『〔｛【";

const STR_TITLE = 'ここに50字までの作品名を入力してください';
const STR_DESCRIPTION = ['本文はここに入力してください。',
	'1作品1000字まで入力できます。',
	'改行毎に一文として認識され、一文中で使える句点の数は一つのみです。',
	'改行や句点の数が不適切な場合、警告が表示され、ボタンが押せない状態になります。'];

var FONT_SIZE;
var canvas;
var context;

function windowResized(){
	let canvas_screen = document.getElementById("canvas_main");
	
	let r = 0.95*window.innerWidth/canvas_screen.width;
	canvas_screen.width *= r;
	canvas_screen.height *= r;
	
	onEditingText();
}

function init(){
	canvas = document.createElement("canvas");
	canvas.width = 1024;
	
	context = canvas.getContext('2d');
	
	document.form1.work_title.value = STR_TITLE;
	
	let str = '';
	for (let s of STR_DESCRIPTION){
		str += s+'\n';
	}
	document.form1.work_text.value = str.slice(0,-1);
	
	let width = canvas.width;
	FONT_SIZE = width/(N_CHAR_OF_ONE_ROW+3);

	windowResized();
	onEditingText();
	
	window.onresize = windowResized;
}

function onEditingText(){
	let alert = document.getElementById('alert');
	alert.innerText = '　';
	
	let str = document.form1.work_text.value;
	let len = str.replace(/\n/g,'').length;
	document.getElementById('length').innerText = len+'文字';
	
	if (len<1 || len>1000){
		alert.innerText = '本文は1字以上1000字以下にしてください。';
		return;
	}
	let sentences = str.split('\n');

	convertToCanvas(sentences);

	let title = document.form1.work_title.value;
	if (title!=null){		
		if(title.length==0|| title.length>50){
			alert.innerText = 'タイトルを1字以上50字以内でつけてください。';
		}else if (title==STR_TITLE){
			alert.innerText = 'タイトルを入力してください。';
		}
	}
	
	if (sentences.length!=4){
		alert.innerText ='改行の数を調整して、四文にしてください。';
	}else{
		for (let i in sentences){
			let periods = sentences[i].match(/。/g);
			if (periods!=null && periods.length>1){
				alert.innerText = '一文中の句点（。）の数は一つまでです。';
			}
			
			if(sentences[i]==STR_DESCRIPTION[i]){
				alert.innerText = '本文を入力してください。';
			}
		}
	}
	
	document.getElementById('save').disabled = (alert.innerText !== '　');
}

function convertToCanvas(sentences){
	makeCanvas(sentences);
	drawBackground();
	drawCaption();
	drawTitle();
	drawText(sentences);
	
	let screen_canvas = document.getElementById("canvas_main");
	let r = canvas.height/canvas.width;
	screen_canvas.height = r*screen_canvas.width;
	
	screen_canvas.getContext('2d').drawImage(canvas, 0,0,canvas.width, canvas.height,
			0,0,screen_canvas.width, screen_canvas.height);
}

function makeCanvas(sentences){
	let nRow =0;
	for (let sentence of sentences){
		let str = insertReturn(sentence, N_CHAR_OF_ONE_ROW);
		let rows = str.slice(0,-1).split('\n');
		nRow += rows.length;
	}
	
	let height = (3.6+nRow)*FONT_SIZE*LINE_HEIGHT;
	
	canvas.height = height;
}

function drawBackground(){
	context.fillStyle = "rgb(255, 255, 255)";
    context.fillRect(0,0,canvas.width,canvas.height);
    
    let half = FONT_SIZE/2;
    context.fillStyle = "rgba(0, 0, 0, 0)";
    context.fillRect(0,0,canvas.width,FONT_SIZE*2*LINE_HEIGHT);
    context.fillRect(0,canvas.height-half,canvas.width,half);
           
    let r = 3*FONT_SIZE;
    context.fillStyle = "rgba(0,120,255,0.08)" ;
    fillCircle(r, r, r) ;
    fillCircle(canvas.width-r, r, r) ;
    fillCircle(r, canvas.height-r, r) ;
    fillCircle(canvas.width-r, canvas.height-r, r) ;
}

function drawCaption(){
	let half = FONT_SIZE/2;

	context.fillStyle = "rgb(234, 234, 234)";
	context.fillRect(0,0,canvas.width,FONT_SIZE);
	
	context.fillStyle = "rgb(0,0,0)";
    context.font = half+'px san-serif';
    context.textBaseline = 'top';

    let caption = '四文で起承転結の【四文転結】';
    let captionWidth = context.measureText(caption).width;
    context.fillText(caption, canvas.width-captionWidth-FONT_SIZE*1.5, FONT_SIZE/4);

    context.fillStyle = "rgb(255, 255, 255)";
    fillCircle(half, half, half/2) ;
    fillCircle(canvas.width-half, half, half/2) ;
    
    context.fillStyle = "rgb(0, 0, 0)";
	let r = FONT_SIZE*0.08;
    let sx = FONT_SIZE*1.5;
	let ex = canvas.width-captionWidth-FONT_SIZE*2;
    for (let x=sx; x<ex; x+=r*5){
       	let ratio = (x-(sx+r))/(ex-(sx+r));
    	context.fillStyle = blendColor([100,100,100],[0,0,0],ratio) ;
    	fillCircle(x,half,r);
    }
}

function fillCircle(x,y,r){
	setCircle(x,y,r);
	context.fill();
}

function strokeCircle(x,y,r){
	setCircle(x,y,r);
	context.stroke();
}

function setCircle(x,y,r){
    context.beginPath () ;
    context.arc(x, y, r, 0 , 2 * Math.PI, true) ;
    context.closePath();
}

function drawTitle(){
	context.fillStyle = "rgb(0,0,0)";
	context.textBaseline = 'top';
    context.font = 'bold '+ FONT_SIZE+'px serif';
    let pre_str = document.form1.work_title.value;
    let str = '『 '+pre_str.split('').join(' ')+' 』';
    let length = context.measureText(str).width;
    let half = length/2;
    let c = canvas.width/2;
    let p = 1.4*FONT_SIZE;
	context.fillText(str, p, p, canvas.width-p*2);

	let r = FONT_SIZE*0.13;
    let y = 2*FONT_SIZE;
	context.lineWidth = 1;
	
    let sx = FONT_SIZE*1.5+length;
	let ex = canvas.width-FONT_SIZE;
    for (let x=sx+r; x<ex; x+=r*5){
    	let ratio = (x-(sx+r))/(ex-(sx+r));
    	context.strokeStyle = blendColor([0,0,0],[90,120,255],ratio) ;
    	strokeCircle(x,y,r);
    }
}

function blendColor(c1, c2, ratio){
	let result = [0,0,0];
	for (let i=0; i<3; i++){
		result[i] = Math.floor(c1[i]*(1-ratio)+c2[i]*ratio);
	}
	return "rgb("+result[0]+","+result[1]+","+result[2]+")";
}

function drawText(sentences){
	context.font = FONT_SIZE+'px serif';
	context.fillStyle = "rgba(0,0,0,0.75)" ;
	
	let offsetX = 1.5;
    let cy=3.3;
    for (let y in sentences){
    	let cx = 0;
    	let chars = insertReturn(sentences[y], N_CHAR_OF_ONE_ROW).split('');
    	for (let x in chars){
    		if (chars[x]=='\n'){
    			cx = 0;
    			if (x<chars.length-1){
    				cy += LINE_HEIGHT;
    			}
    		}else{
	    		let px = (offsetX+cx)*FONT_SIZE;
	    		let py = cy * FONT_SIZE;
	    		let len = context.measureText(chars[x]).width;
	        	context.fillText(chars[x], px+(FONT_SIZE-len)/2, py);
	        	cx++;
    		}
    	}
    	cy += LINE_HEIGHT+0.5;
    }
    
    context.fillStyle = "rgba(0, 0, 0, 0.1)";
    let half = FONT_SIZE/2;
    fillCircle(half, canvas.height-half, half/2) ;
    fillCircle(canvas.width-half, canvas.height-half, half/2) ;
}

function insertReturn( strInput, intLineLength ){
	if (strInput.length<=intLineLength){
		return strInput;
	}
	
	let chs = strInput.split('');
    var result = "";
    let temp = "";
    for(let i=0; i<chs.length; i++){
        temp += chs[i];
        if( temp.length == intLineLength ){
          result += temp;
          temp ="";
          // この文字が行末禁止文字→改行
          if( ~BAN_HEAD.indexOf(chs[i])){
        	  result = result.slice(0,-1);
        	  temp = chs[i];
          }
          // 次の文字が行頭禁止文字→ぶらさがり処理
          if( ~BAN_FOOT.indexOf(chs[i+1])){
        	// 次の文字とその次が行頭禁止文字→改行
        	  if( ~BAN_FOOT.indexOf(chs[i+2])){
        		  	result = result.slice(0,-1);
                  	temp = chs[i]+chs[i+1]+chs[i+2];
                  	i+=2;  
        	  }else{
	            result += chs[i+1];
	            i++;
        	  }
          }
          result += "\n";
        }
      }
    result += temp;
    return result;
}
  
 function onSave(){
	 canvas.toBlob(function(blob) {
		const name = '四文転結.png';
		if (window.navigator.msSaveBlob) {
			window.navigator.msSaveBlob(blob, name);
		} else {
			let link = document.createElement('a');
			link.download = name;
			link.href = window.URL.createObjectURL(blob);
			link.click();
		}
	});
 }
 

